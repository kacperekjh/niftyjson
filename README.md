
NiftyJSON
==========
***A simple to use C++ JSON Serializer***

# 1. Description
NiftyJSON is a JSON serializer that was designed with ease of use in mind. It aims to minimize the amount of boilerplate while being as fast as possible.

The project had been initially created for the purpose of learning metaprogramming, but soon after I changed my focus and decided to make a JSON parsing library intended to be as easy to use as it could ever be.

# 2. Features
NiftyJSON currently supports the serialization and deserialization of:
- Arithmetic types, including booleans and underlying types of enums
- All standard containers
- Strings
- User-defined objects
- Dynamically allocated objects

Furthermore, NiftyJSON allows you to easily add user-defined serializers for other non-object types.

# 3. Usage
## Serialization and deserialization
To begin serializing objects, you first need to include the header [serializer.h](serialization/serializer.h).
Serializing objects is as simple as calling `JSONSerializer::serialize(object)`. This function will return an object containing a pointer to the serialized string and its size.
To deserialize an object, you use `JSONSerializer::deserialize(object, data, length)`.
Currently, the data string for deserialization cannot contain any whitespace characters outside of strings; if it does, you need to call `JSONSerializer::removeWhitespaces(string)` first and then use its output.

## Making objects serializable
Serializing user-defined objects requires you to declare their fields first. To do so, you need to use the `DECLARE_FIELDS(class, fields...)` macro after defining member variables, where `class` is the name of the class that you are declaring fields for and `fields...` is a list of all the fields that you want to be serializable. A field can be defined using either of these two macros:
-  `FIELD(member)` if you want to serialize `member` under the same name you use in code.
-  `NAMED_FIELD(member, name)` in case you want to serialize `member` as `name`.

For example:
```c++
class  Serializable {
int a = 0, b = 1, d = 2;
DECLARE_FIELDS(Serializable, FIELD(a), FIELD(b), NAMED_FIELD(d, c));
};
```
Serializing a default-constructed object of class `Serializable` will return: `{"a":0,"b":1,"c":2}`.
Additionally, serializable objects may define these functions to be called during (de)serialization:
```c++
void onSerialize() const;
void onSerialized() const;
void onDeserialize();
void onDeserialized();
```

## Writing custom serializers
To serialize other types that aren't objects, you need to write a specialization of `JSONSerializer::Serializer` for the custom type. In order to be usable, the specialization must define these two static functions:
```c++
static Serialized serialize(std::add_const_t<T> &object);
static void deserialize(std::remove_const_t<T> &object, const char *data, size_t size);
```

## Known bugs
None yet.
