#ifndef _SERIALIZER_
#define _SERIALIZER_

#include <concepts>
#include <stdexcept>
#include "serialized.h"

namespace JSONSerializer
{
    template <class T>
    struct Serializer {};

    template <class T>
    concept HasSerializer = requires(std::add_const_t<T> cv, std::remove_const_t<T> v, const char *data, size_t size)
    {
        { Serializer<T>::serialize(cv) } -> std::same_as<Serialized>;
        { Serializer<T>::deserialize(v, data, size) };
    };

    template <class T>
    Serialized serialize(const T &field);
    template <class T>
    void deserialize(T &field, const char *data, size_t size);

    /// @brief Removes all whitespace characters from a JSON string. This function has to be used for formatted JSON data to avoid ParseErrors.
    /// @param data Whitespace-containing JSON string
    /// @return JSON string without whitespace characters
    std::string removeWhitespaces(const std::string_view &data);

    struct ParseError : std::runtime_error
    {
        inline static const char *dataStart = nullptr;

        ParseError(const std::string &message, const char *position) : std::runtime_error(message + " at position " + std::to_string(position - dataStart) + '.') {}
        ParseError(const char *message, const char *position) : ParseError(std::string(message), position) {}
    };
}

#include "serializers/tupleserializer.h"
#include "serializers/mapserializer.h"
#include "serializers/arithmeticserializer.h"
#include "serializers/pointerserializer.h"
#include "serializers/stringserializer.h"
#include "serializers/objectserializer.h"
#include "serializers/collectionserializer.h"
#include "serializers/enumserializer.h"
#include "serializers/stackserializer.h"

#include "serializer.hxx"

#endif