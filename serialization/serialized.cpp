#include "serialized.h"

namespace JSONSerializer
{
    Serialized::operator std::string() const
    {
        return std::string(str, size);
    }

    Serialized::operator std::string_view() const
    {
        return std::string_view(str, size);
    }
}