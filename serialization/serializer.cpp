#include "serializer.h"
#include "helper.h"

namespace JSONSerializer
{
    std::string removeWhitespaces(const std::string &data)
    {
        std::string clean = {};
        clean.reserve(data.size());
        const char *endingQuote = nullptr;
        const char *ptr = data.c_str();
        const char *end = ptr + data.size();
        while (ptr < end)
        {
            if (*ptr != '\n' && *ptr != '\r' && *ptr != '\t' && (ptr < endingQuote || *ptr != ' '))
            {
                clean += *ptr;
            }
            if (ptr != endingQuote && *ptr == '\"')
            {
                endingQuote = Helper::findNext('\"', ptr + 1, end);
            }
            ptr++;
        }
        return clean;
    }
}