#include "serializer.h"
#include <iostream>
#include <array>
#include <vector>
#include <map>
#include <unordered_set>
#include <stack>
#include <tuple>

enum Enum : unsigned char
{
    Value1 = 1,
    Value2 = 3,
    Value3 = 5
};

class SubClass
{
public:
    float number = -2.34f;
    Enum _enum = Value2;

    // For an object to be serializable, it is required to have a zero-argument constructor and have its fields
    // defined using DECLARE_FIELDS, FIELD and NAMED_FIELD macros.
    // FIELD(x) serializes member 'x' without changing its name in JSON i.e. FIELD(number) -> "number":-2.340000
    // NAMED_FIELD(x, y) serializes member 'x' using 'y' as its name i.e. NAMED_FIELD(_enum, enum) -> "enum":3
    DECLARE_FIELDS(SubClass, FIELD(number), NAMED_FIELD(_enum, enum));
};

class Class
{
public:
    // The serializer supports all standard container types, arithmetic types, enums and tuples.
    std::string text = {};
    std::array<unsigned short, 4> array = {};
    std::vector<long double> vector = {};
    SubClass sub = {};
    std::map<std::string, SubClass*> subs = {}; // Pointer types are always allocated on the heap or set to nullptr during deserialization
    std::unordered_multiset<float> set = {};
    std::stack<bool> stack = {};
    std::tuple<int, float, std::string> tuple = {};

    DECLARE_FIELDS(Class, FIELD(text), FIELD(array), FIELD(vector), FIELD(sub), FIELD(subs), FIELD(set), FIELD(stack), FIELD(tuple));

    ~Class()
    {
        for (auto &sub : subs)
        {
            delete sub.second;
        }
    }

    // The serializer allows for these four optional functions to be called during serialization or deserialization.
    // onSerialize() is called right before the object gets serialized. It must be a const function.
    void onSerialize() const
    {
        std::cout << "Serializing\n";
    }

    // onSerialized() is called after the object gets serialized. Like onSerialize(), it has to be const.
    void onSerialized() const
    {
        std::cout << "Serialized\n";
    }

    // onDeserialize() is called before the object gets deserialized. It doesn't have to be const.
    void onDeserialize()
    {
        std::cout << "Deserializing\n";
        for (auto &sub : subs)
        {
            delete sub.second;
        }
    }

    // onDeserialized() is called after the object gets deserialized. It also doesn't have to be const.
    void onDeserialized()
    {
        std::cout << "Deserialized\n";
    }
};

// Objects can also be serialized and deserialized using custom functions when there's a specialization of
// JSONSerializer::Serializer for their type. The specialization is required to have these two functions:
// static Serialized serialize(std::add_const_t<T> &object);
// static void deserialize(std::remove_const_t<T> &object, const char *data, size_t size);
// Specializations for every supported type can be found in './serializers'.

int main()
{
    Class object = {};
    object.text = "\n\"sample\"\ntext\r";
    object.array = {4, 3, 2, 1};
    object.subs = {{"sub1", new SubClass()}, {"sub2", nullptr}, {"sub3", nullptr}};
    object.set = {12.2, 12.2, 3};
    object.stack.push(true);
    object.stack.push(false);
    object.stack.push(true);
    object.tuple = {-3, -89.33f, "string"};
    auto serialized = JSONSerializer::serialize(object);
    std::cout << (std::string_view)serialized << '\n';

    Class *object2 = nullptr;
    JSONSerializer::deserialize(object2, serialized.str, serialized.size);
    std::cout << "object2->text: " << object2->text << '\n';
    delete object2;
    delete[] serialized.str;
    return 0;
}