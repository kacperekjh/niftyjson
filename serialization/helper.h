#ifndef _HELPER_
#define _HELPER_

#include <vector>
#include <string>
#include "serialized.h"

namespace JSONSerializer::Helper
{
    template <class T, template<typename...> class Ref>
    struct is_specialization : std::false_type {};

    template <template<class...> class Ref, class... Args>
    struct is_specialization<Ref<Args...>, Ref>: std::true_type {};

    template <class T, template<typename...> class Ref>
    const bool is_specialization_v = is_specialization<T, Ref>::value;

    /// @brief Finds the first occurance of char `toFind` while skipping content in {}, [] and "".
    /// @param toFind The character to search for
    /// @param data The beginning of the string
    /// @param end One byte past the last character of the string
    /// @return Pointer to the first occurance of `toFind` or nullptr if it's not present in the string
    const char *findNext(char toFind, const char *data, const char *end);

    /// @brief Returns list of all parts of `data` separated by commas.
    /// @param data The string to split
    /// @param size Size of the string
    /// @return std::vector containing beginnings and sizes of parts
    std::vector<std::pair<const char *, size_t>> getFields(const char *data, size_t size);

    /// @brief Calculates length of a string after escaping all special characters.
    /// @param string The string to calculate the length of
    /// @return Length of the escaped string
    size_t getEscapedStringLength(const std::string &string);

    /// @brief Creates a string with all special characters escaped.
    /// @param string The string to escape
    /// @return Data of the escaped string
    JSONSerializer::Serialized escapeString(const std::string &string);

    /// @brief Parsed data of an escaped string
    /// @param data Beginning of the escaped string
    /// @param end Pointer one byte past the last character of the string
    /// @return String with all escape sequences parsed
    std::string parseString(const char *data, size_t size);

    /// @brief Parses an escape sequence
    /// @param chars Beginning of the escape sequence
    /// @param end Pointer one byte past the last character of the parsed string
    /// @param sequenceLength Length of the sequence
    /// @param charsCount Amount of returned characters
    /// @return Parsed escape sequence
    char *escapeChars(const char *chars, const char *end, size_t &sequenceLength, size_t &charsCount);

    /// @brief Returns a number corresponding to the hexadecimal digit
    /// @param octal The digit to parse
    /// @return Parsed number
    unsigned char parseHexDigit(char hex);

    /// @brief Converts Unicode codepoint into 1-4 UTF-8 bytes
    /// @param chars Pointer to the first character of the hex codepoint
    /// @param end Pointer one byte past the last character of the parsed string
    /// @param sequenceLength Length of the codepoint(s)
    /// @param charsCount Amount of returned characters
    /// @return UTF-8 bytes for the corresponding codepoint
    char *parseCodePoint(const char *chars, const char *end, size_t &sequenceLength, size_t &charsCount);

    template <size_t Size>
    struct StringWrapper
    {
        char string[Size];

        constexpr StringWrapper(const char (&str)[Size])
        {
            for (size_t i = 0; i < Size; i++)
            {
                string[i] = str[i];
            }
        }
    };
}

#endif