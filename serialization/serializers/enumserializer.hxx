namespace JSONSerializer
{
    template <IsEnum T>
    Serialized Serializer<T>::serialize(std::add_const_t<T> &object)
    {
        return JSONSerializer::serialize<typename std::underlying_type_t<T>>(object);
    }
    
    template <IsEnum T>
    void Serializer<T>::deserialize(std::remove_const_t<T> &object, const char *data, size_t size)
    {
        using Type = typename std::underlying_type_t<T>;
        JSONSerializer::deserialize<Type>((Type&)object, data, size);
    }
}