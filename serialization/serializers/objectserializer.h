#ifndef _OBEJCTSERIALIZER
#define _OBEJCTSERIALIZER

#include "../serializer.h"
#include <string.h>

#define FIELD(member) JSONSerializer::Field<this_type, decltype(member), &this_type::member, #member>
#define NAMED_FIELD(member, name) JSONSerializer::Field<this_type, decltype(member), &this_type::member, #name>
#define DECLARE_FIELDS(class, fs...) \
using this_type = class;             \
using fields = JSONSerializer::Fields<class, fs>

namespace JSONSerializer
{
    struct SerializedField
    {
        char *data;
        size_t dataSize;
        const char *name;
        size_t nameSize;
    };

    template <class Owner, class Member, Member Owner::*Pointer, Helper::StringWrapper Name>
    struct Field
    {
        using member_type = Member;
        static constexpr Member Owner::*pointer = Pointer;
        static constexpr Helper::StringWrapper name = Name;
    };

    template <class Owner, class Field, class... Rest>
    class Fields
    {
    private:
        static void serializeRest(std::add_const_t<Owner> &object, SerializedField *fields);

    public:
        static constexpr size_t size = sizeof...(Rest) + 1;
        static SerializedField *serializeFields(std::add_const_t<Owner> &object);
        static void deserializeField(std::remove_const_t<Owner> &object, const char *fieldName, size_t nameSize, const char *data, size_t size);

        template <class, class, class...>
        friend class Fields;
    };

    template <class T>
    concept IsSerializableObject = requires(std::add_const_t<T> cv, std::remove_const_t<T> v, const char *fieldName, size_t nameSize, const char *data, size_t size)
    {
        { T::fields::serializeFields(cv) } -> std::same_as<SerializedField*>;
        T::fields::deserializeField(v, fieldName, nameSize, data, size);
        T();
    };

    template <IsSerializableObject T>
    struct Serializer<T>
    {
        static Serialized serialize(std::add_const_t<T> &object);
        static void deserialize(std::remove_const_t<T> &object, const char *data, size_t size);
    };
}

#include "objectserializer.hxx"

#endif