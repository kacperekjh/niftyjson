#ifndef _STACKSERIALIZER_
#define _STACKSERIALIZER_

#include "../serializer.h"

namespace JSONSerializer
{
    template <class T>
    concept IsStack = requires (std::remove_const_t<T> v, typename T::value_type value)
    {
        typename T::container_type;
    };

    template <IsStack T>
    struct StackAdapter : T
    {
        using T::c;
    };

    template <IsStack T>
    struct Serializer<T>
    {
        static Serialized serialize(std::add_const_t<T> &object);
        static void deserialize(std::remove_const_t<T> &object, const char *data, size_t size);
    };
};

#include "stackserializer.hxx"

#endif