#ifndef _COLLECTIONSERIALIZER_
#define _COLLECTIONSERIALIZER_

#include "../serializer.h"
#include <string.h>

namespace JSONSerializer
{
    template <class T>
    concept IsCollection = requires(T v)
    {
        typename T::value_type;
        v.begin();
        v.end();
    } && !IsTuple<T> && !IsString<T> && !IsMap<T>;

    template <class T>
    concept IsDynamicCollection = requires(T v, typename T::value_type e) {
        v.insert(v.begin(), e);
    };

    template <class T>

    concept IsFixedCollection = requires(T v, size_t i) {
        v.at(i);
    };

    template <IsCollection T>
    struct Serializer<T>
    {
        static Serialized serialize(std::add_const_t<T> &object);
        static void deserialize(std::remove_const_t<T> &object, const char *data, size_t size);
    };
}

#include "collectionserializer.hxx"

#endif