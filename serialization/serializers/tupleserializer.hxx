namespace JSONSerializer
{
    template <IsTuple T>
    template <size_t I>
    void TupleSerializer<T>::getFields(std::add_const_t<T> &tuple, Serialized *fields)
    {
        fields[I] = JSONSerializer::serialize<std::tuple_element_t<I, T>>(std::get<I>(tuple));
        if constexpr (I < std::tuple_size_v<T> - 1)
        {
            getFields<I + 1>(tuple, fields);
        }
    }

    template <IsTuple T>
    template <size_t I>
    void TupleSerializer<T>::setFields(std::remove_const_t<T> &tuple, std::vector<std::pair<const char *, size_t>> &fields)
    {
        JSONSerializer::deserialize<std::tuple_element_t<I, T>>(std::get<I>(tuple), fields[I].first, fields[I].second);
        if constexpr (I < std::tuple_size_v<T> - 1)
        {
            setFields<I + 1>(tuple, fields);
        }
    }

    template <IsTuple T>
    Serialized TupleSerializer<T>::serialize(std::add_const_t<T> &tuple)
    {
        Serialized fields[std::tuple_size_v<T>];
        TupleSerializer<T>::getFields(tuple, fields);
        size_t size = 2; // []
        for (const auto &field : fields)
        {
            size += field.size;
        }
        if constexpr (std::tuple_size_v<T> > 1)
        {
            size += std::tuple_size_v<T> - 1; // ,
        }
        char *data = new char[size];
        data[0] = '[';
        data[size - 1] = ']';
        char *ptr = data + 1;
        bool first = true;
        for (auto &field : fields)
        {
            if (!first)
            {
                *(ptr++) = ',';
            }
            first = false;
            memcpy(ptr, field.str, field.size);
            ptr += field.size;
            delete[] field.str;
        }
        return {data, size};
    }

    template <IsTuple T>
    void TupleSerializer<T>::deserialize(std::remove_const_t<T> &tuple, const char *data, size_t size)
    {
        if (size < 2 || data[0] != '[' || data[size - 1] != ']')
        {
            throw ParseError("Missing square brackets", data);
        }
        auto fields = Helper::getFields(data + 1, size - 2);
        if (fields.size() != std::tuple_size_v<T>)
        {
            throw ParseError("Invalid amount of tuple elements (" + std::to_string(std::tuple_size_v<T>) + " required)", data);
        }
        setFields(tuple, fields);
    }

    template <IsTuple T>
    Serialized Serializer<T>::serialize(std::add_const_t<T> &object)
    {
        return TupleSerializer<T>::serialize(object);
    }

    template <IsTuple T>
    void Serializer<T>::deserialize(std::remove_const_t<T> &object, const char *data, size_t size)
    {
        TupleSerializer<T>::deserialize(object, data, size);
    }
}