#ifndef _STRINGSERIALIZER_
#define _STRINGSERIALIZER_

#include "../serializer.h"

namespace JSONSerializer
{
    template <class T>
    concept IsString = std::is_same_v<T, std::string>;

    template <IsString T>
    struct Serializer<T>
    {
        static Serialized serialize(std::add_const_t<T> &object);
        static void deserialize(std::remove_const_t<T> &object, const char *data, size_t size);
    };
}

#include "stringserializer.hxx"

#endif