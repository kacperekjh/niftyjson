#include "../serializer.h"

namespace JSONSerializer
{
    template <class T>
    concept IsEnum = std::is_enum_v<T>;

    template <IsEnum T>
    struct Serializer<T>
    {
        static Serialized serialize(std::add_const_t<T> &object);
        static void deserialize(std::remove_const_t<T> &object, const char *data, size_t size);
    };
}

#include "enumserializer.hxx"