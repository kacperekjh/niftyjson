#ifndef _TUPLESERIALIZER_
#define _TUPLESERIALIZER_

#include <tuple>
#include <stdexcept>
#include <string.h>
#include "../serializer.h"
#include "../helper.h"

namespace JSONSerializer
{
    template <class T>
    concept IsTuple = requires(T v, size_t s)
    {
        std::get<0>(v);
        std::tuple_size_v<T>;
    };

    template <IsTuple T>
    class TupleSerializer
    {
    private:
        template <size_t I = 0>
        static void getFields(std::add_const_t<T> &tuple, Serialized *fields);
        template <size_t I = 0>
        static void setFields(std::remove_const_t<T> &tuple, std::vector<std::pair<const char *, size_t>> &fields);

    public:
        static Serialized serialize(std::add_const_t<T> &tuple);
        static void deserialize(std::remove_const_t<T> &tuple, const char *data, size_t size);

        template <IsTuple>
        friend class TupleSerializer;
    };

    template <IsTuple T>
    struct Serializer<T>
    {
        static Serialized serialize(std::add_const_t<T> &object);
        static void deserialize(std::remove_const_t<T> &object, const char *data, size_t size);
    };
}

#include "tupleserializer.hxx"

#endif