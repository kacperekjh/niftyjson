#ifndef _SIMPLESERIALIZER_
#define _SIMPLESERIALIZER_

#include "../serializer.h"
#include <string.h>

namespace JSONSerializer
{
    template <class T>
    concept IsArithmetic = std::is_arithmetic_v<T>;

    template <IsArithmetic T>
    struct Serializer<T>
    {
        static Serialized serialize(std::add_const_t<T> &object);
        static void deserialize(std::remove_const_t<T> &object, const char *data, size_t size);
    };
}

#include "arithmeticserializer.hxx"

#endif