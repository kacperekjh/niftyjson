namespace JSONSerializer
{
    template <IsMap T>
    Serialized Serializer<T>::serialize(std::add_const_t<T> &object)
    {
        std::vector<PairType> elements = std::vector<PairType>(object.size());
        size_t i = 0;
        for (const auto &element : object)
        {
            elements[i++] = {element.first, element.second};
        }
        return JSONSerializer::serialize(elements);
    }

    template <IsMap T>
    void Serializer<T>::deserialize(std::remove_const_t<T> &object, const char *data, size_t size)
    {
        std::vector<PairType> elements;
        JSONSerializer::deserialize(elements, data, size);
        object.clear();
        for (const auto &element : elements)
        {
            object.insert(element);
        }
    }
}