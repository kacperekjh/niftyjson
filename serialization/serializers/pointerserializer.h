#ifndef _POINTERSERIALIZER_
#define _POINTERSERIALIZER_

#include "../serializer.h"

namespace JSONSerializer
{
    template <class T>
    concept IsPointer = std::is_pointer_v<T>;

    template <IsPointer T>
    struct Serializer<T>
    {
        static Serialized serialize(std::add_const_t<T> &object);
        static void deserialize(std::remove_const_t<T> &object, const char *data, size_t size);
    };
}

#include "pointerserializer.hxx"

#endif