namespace JSONSerializer
{
    template <IsPointer T>
    Serialized Serializer<T>::serialize(std::add_const_t<T> &object)
    {
        if (object)
        {
            return JSONSerializer::serialize(*object);
        }
        else
        {
            return {new char[4]{'n', 'u', 'l', 'l'}, 4};
        }
    }

    template <IsPointer T>
    void Serializer<T>::deserialize(std::remove_const_t<T> &object, const char *data, size_t size)
    {
        if (size == 4 && memcmp(data, "null", 4) == 0)
        {
            object = nullptr;
        }
        else
        {
            object = new std::remove_pointer_t<T>{};
            try
            {
                JSONSerializer::deserialize(*object, data, size);
            }
            catch (const ParseError &e)
            {
                delete object;
                object = nullptr;
                throw e;
            }
        }
    }
}