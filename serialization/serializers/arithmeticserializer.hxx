#include <charconv>

namespace JSONSerializer
{
    template <IsArithmetic T>
    Serialized Serializer<T>::serialize(std::add_const_t<T> &object)
    {
        if constexpr (std::is_same_v<T, bool>)
        {
            if (object)
            {
                return {new char[4]{'t', 'r', 'u', 'e'}, 4};
            }
            else
            {
                return {new char[5]{'f', 'a', 'l', 's', 'e'}, 5 };
            }
        }
        else
        {
            constexpr size_t bufSize = 32;
            char buf[bufSize];
            auto result = std::to_chars(buf, buf + bufSize, object);
            size_t size = result.ptr - buf;
            char *data = new char[size];
            memcpy(data, buf, size);
            return {data, size};
        }
    }

    template <IsArithmetic T>
    void Serializer<T>::deserialize(std::remove_const_t<T> &object, const char *data, size_t size)
    {
        if constexpr (std::is_same_v<T, bool>)
        {
            if (size == 4 && memcmp(data, "true", 4) == 0)
            {
                object = true;
            }
            else if (size == 5 && memcmp(data, "false", 5) == 0)
            {
                object = false;
            }
            else
            {
                throw ParseError("Invalid boolean value", data);
            }
        }
        else
        {
            auto result = std::from_chars(data, data + size, object);
            if (result.ec != std::errc())
            {
                throw ParseError("Invalid value", data);
            }
        }
    }
}
