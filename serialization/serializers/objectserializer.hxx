namespace JSONSerializer
{
    template <class Owner, class Field, class... Rest>
    void Fields<Owner, Field, Rest...>::serializeRest(std::add_const_t<Owner> &object, SerializedField *fields)
    {
        Serialized data = JSONSerializer::serialize(object.*Field::pointer);
        fields[0] = {data.str, data.size, Field::name.string, sizeof(Field::name.string) - 1};
        if constexpr (sizeof...(Rest) > 0)
        {
            Fields<Owner, Rest...>::serializeRest(object, fields + 1);
        }
    }

    template <class Owner, class Field, class... Rest>
    SerializedField *Fields<Owner, Field, Rest...>::serializeFields(std::add_const_t<Owner> &object)
    {
        SerializedField *fields = new SerializedField[size];
        serializeRest(object, fields);
        return fields; 
    }

    template <class Owner, class Field, class... Rest>
    void Fields<Owner, Field, Rest...>::deserializeField(std::remove_const_t<Owner> &object, const char *fieldName, size_t nameSize, const char *data, size_t size)
    {
        if (strncmp(fieldName, Field::name.string, nameSize) == 0)
        {
            JSONSerializer::deserialize(object.*Field::pointer, data, size);
            return;
        }
        if constexpr (sizeof...(Rest) > 0)
        {
            Fields<Owner, Rest...>::deserializeField(object, fieldName, nameSize, data, size);
        }
        else
        {
            throw ParseError("Unknown field '" + std::string(fieldName, nameSize) + "'", fieldName);
        }
    }

    template <IsSerializableObject T>
    Serialized Serializer<T>::serialize(std::add_const_t<T> &object)
    {
        if constexpr (requires() { object.onSerialize(); })
        {
            object.onSerialize();
        }
        size_t size = T::fields::size * 4 + 1;
        SerializedField *elementsData = T::fields::serializeFields(object);
        for (size_t i = 0; i < T::fields::size; i++)
        {
            size += elementsData[i].dataSize + elementsData[i].nameSize;
        }

        char *data = new char[size];
        data[0] = '{';
        data[size - 1] = '}';
        char *ptr = data + 1;
        bool first = true;
        for (size_t i = 0; i < T::fields::size; i++)
        {
            if (!first)
            {
                *(ptr++) = ',';
            }
            first = false;
            *(ptr++) = '"';
            memcpy(ptr, elementsData[i].name, elementsData[i].nameSize);
            ptr += elementsData[i].nameSize;
            *(ptr++) = '"';
            *(ptr++) = ':';
            memcpy(ptr, elementsData[i].data, elementsData[i].dataSize);
            ptr += elementsData[i].dataSize;
            delete[] elementsData[i].data;
        }
        delete[] elementsData;
        if constexpr (requires() { object.onSerialized(); })
        {
            object.onSerialized();
        }
        return {data, size};
    }

    template <IsSerializableObject T>
    void Serializer<T>::deserialize(std::remove_const_t<T> &object, const char *data, size_t size)
    {
        if (size < 2 || data[0] != '{' || data[size - 1] != '}')
        {
            throw ParseError("Missing curly brackets", data);
        }
        if constexpr (requires() { object.onDeserialize(); })
        {
            object.onDeserialize();
        }
        auto fields = Helper::getFields(data + 1, size - 2);
        for (auto &field2 : fields)
        {
            const char *colon = Helper::findNext(':', field2.first, field2.first + field2.second);
            if (!colon)
            {
                throw ParseError("Missing colon", field2.first);
            }
            size_t nameLength = colon - field2.first;
            if (nameLength < 2 || field2.first[0] != '\"' || colon[-1] != '\"')
            {
                throw ParseError("Missing field name", field2.first);
            }
            T::fields::deserializeField(object, field2.first + 1, nameLength - 2, colon + 1, field2.first + field2.second - colon - 1);
        }
        if constexpr (requires() { object.onDeserialized(); })
        {
            object.onDeserialized();
        }
    }
}