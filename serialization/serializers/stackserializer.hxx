namespace JSONSerializer
{
    template <IsStack T>
    Serialized Serializer<T>::serialize(std::add_const_t<T> &object)
    {
        return JSONSerializer::serialize(static_cast<const StackAdapter<T>&>(object).c);
    }

    template <IsStack T>
    void Serializer<T>::deserialize(std::remove_const_t<T> &object, const char *data, size_t size)
    {
        JSONSerializer::deserialize(static_cast<StackAdapter<T>&>(object).c, data, size);
    }
}