namespace JSONSerializer
{
    template <IsCollection T>
    Serialized Serializer<T>::serialize(std::add_const_t<T> &object)
    {
        size_t elementCount = object.size();
        Serialized *elementsData = new Serialized[elementCount];
        size_t size = 2; // []
        size_t index = 0;
        for (const auto &element : object)
        {
            elementsData[index] = JSONSerializer::serialize(element);
            size += elementsData[index].size;
            index++;
        }
        if (elementCount > 1)
        {
            size += elementCount - 1; // ,
        }

        char *data = new char[size];
        data[0] = '[';
        data[size - 1] = ']';
        char *ptr = data + 1;
        bool first = true;
        for (size_t i = 0; i < elementCount; i++)
        {
            if (!first)
            {
                *(ptr++) = ',';
            }
            first = false;
            memcpy(ptr, elementsData[i].str, elementsData[i].size);
            delete[] elementsData[i].str;
            ptr += elementsData[i].size;
        }
        delete[] elementsData;
        return {data, size};
    }

    template <IsCollection T>
    void Serializer<T>::deserialize(std::remove_const_t<T> &object, const char *data, size_t size)
    {
        if (size < 2 || data[0] != '[' || data[size - 1] != ']')
        {
            throw ParseError("Missing square brackets", data);
        }
        object = {};
        auto elements = Helper::getFields(data + 1, size - 2);
        if (elements.size() > object.max_size())
        {
            throw ParseError("Too many collection elements", data);
        }
        static_assert(IsDynamicCollection<T> || IsFixedCollection<T>, "Unsupported collection.");
        if constexpr (IsDynamicCollection<T>)
        {
            for (auto &element : elements)
            {
                std::remove_const_t<typename T::value_type> field;
                JSONSerializer::deserialize(field, element.first, element.second);
                object.insert(object.end(), field);
            }
        }
        else
        {
            size_t i = 0;
            for (auto &element : elements)
            {
                std::remove_const_t<typename T::value_type> field;
                JSONSerializer::deserialize(field, element.first, element.second);
                object.at(i) = field;
                i++;
            }
        }
    }
}