namespace JSONSerializer
{
    template <IsString T>
    Serialized Serializer<T>::serialize(std::add_const_t<T> &object)
    {
        return Helper::escapeString(object);
    }

    template <IsString T>
    void Serializer<T>::deserialize(std::remove_const_t<T> &object, const char *data, size_t size)
    {
        if (data[0] != '\"' || data[size - 1] != '\"')
        {
            throw ParseError("Missing double quotes", data);
        }
        object = Helper::parseString(data + 1, size - 2);
    }
}