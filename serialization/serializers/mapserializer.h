#ifndef _MAPSERIALIZER_
#define _MAPSERIALIZER_

#include <map>
#include <unordered_map>
#include <vector>
#include "../serializer.h"
#include "../helper.h"

namespace JSONSerializer
{
    template <class T>
    concept IsMap = Helper::is_specialization_v<T, std::map> || Helper::is_specialization_v<T, std::multimap> || Helper::is_specialization_v<T, std::unordered_map> || Helper::is_specialization_v<T, std::unordered_multimap>;

    template <IsMap T>
    struct Serializer<T>
    {
        using PairType = std::pair<std::remove_const_t<typename T::key_type>, std::remove_const_t<typename T::mapped_type>>;

        static Serialized serialize(std::add_const_t<T> &object);
        static void deserialize(std::remove_const_t<T> &object, const char *data, size_t size);
    };
};

#include "mapserializer.hxx"

#endif