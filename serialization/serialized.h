#ifndef _SERIALIZED_
#define _SERIALIZED_

#include <cstddef>
#include <string>

namespace JSONSerializer
{
    /// @brief A container for storing serialized JSON data and its size. It is not responsible for deleting `str`.
    struct Serialized
    {
        char *str;
        size_t size;

        operator std::string() const;
        operator std::string_view() const;
    };
}

#endif