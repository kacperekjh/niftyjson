namespace JSONSerializer
{
    /// @brief Serializes an object.
    /// @tparam T Type of the object
    /// @param field The object to serialize
    /// @return Serialized data
    template <class T>
    Serialized JSONSerializer::serialize(const T &field)
    {
        static_assert(HasSerializer<T>, "Unsupported type for serialization.");
        return Serializer<T>::serialize(field);
    }

    /// @brief Deserializes an object.
    /// @tparam T Type of the object
    /// @param field The object to deserialize
    /// @param data Pointer to an unterminated JSON string
    /// @param size Size of the JSON string
    template <class T>
    void JSONSerializer::deserialize(T &field, const char *data, size_t size)
    {
        static_assert(HasSerializer<T>, "Unsupported type for serializaion.");
        static_assert(!std::is_const_v<T>, "Cannot deserialize const types.");
        bool clearSource = false;
        if (!ParseError::dataStart)
        {
            ParseError::dataStart = data;
            clearSource = true;
        }
        Serializer<T>::deserialize(field, data, size);
        if (clearSource)
        {
            ParseError::dataStart = nullptr;
        }
    }
}