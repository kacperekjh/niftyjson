#include "helper.h"
#include "serializer.h"
#include <cstdint>

namespace JSONSerializer::Helper
{
    const char *findNext(char toFind, const char *data, const char *end)
    {
        while (data < end)
        {
            if (*data == toFind)
            {
                return data;
            }
            else if (*data == '\\')
            {
                data += 2;
            }
            else
            {
                const char *opening = data;
                switch (*data)
                {
                    case '{':
                        data = findNext('}', data + 1, end);
                        break;
                    case '[':
                        data = findNext(']', data + 1, end);
                        break;
                    case '"':
                        data = findNext('"', data + 1, end);
                        break;
                }
                if (!data)
                {
                    throw ParseError(std::string("Unterminated '") + *opening + '\'', opening);
                }
                data++;
            }
        }
        return nullptr;
    }

    std::vector<std::pair<const char *, size_t>> getFields(const char *data, size_t size)
    {
        auto fields = std::vector<std::pair<const char *, size_t>>();
        if (size == 0)
        {
            return fields;
        }
        const char *end = data + size;
        while (data < end)
        {
            const char *comma = findNext(',', data, end);
            if (!comma)
            {
                break;
            }
            fields.push_back({data, comma - data});
            data = comma + 1;
        }
        if (data >= end)
        {
            throw ParseError("Missing element", data);
        }
        fields.push_back({data, end - data});
        return fields;
    }

    size_t getEscapedStringLength(const std::string &string)
    {
        size_t size = 0;
        for (auto c : string)
        {
            switch (c)
            {
            case '"':
            case '\\':
            case '/':
            case '\b':
            case '\f':
            case '\n':
            case '\r':
            case '\t':
                size += 2;
                break;
            default:
                size++;
                break;
            }

        }
        return size;
    }

    Serialized escapeString(const std::string &string)
    {
        size_t size = getEscapedStringLength(string) + 2;
        char *bytes = new char[size]();
        bytes[0] = '\"';
        bytes[size - 1] = '\"';
        char *ptr = bytes + 1;
        for (auto c : string)
        {
            switch (c)
            {
            case '"':
                *(ptr++) = '\\';
                *(ptr++) = '"';
                break;
            case '\\':
                *(ptr++) = '\\';
                *(ptr++) = '\\';
                break;
            case '/':
                *(ptr++) = '/';
                *(ptr++) = '"';
                break;
            case '\b':
                *(ptr++) = '\\';
                *(ptr++) = 'b';
                break;
            case '\f':
                *(ptr++) = '\\';
                *(ptr++) = 'f';
                break;
            case '\n':
                *(ptr++) = '\\';
                *(ptr++) = 'n';
                break;
            case '\r':
                *(ptr++) = '\\';
                *(ptr++) = 'r';
                break;
            case '\t':
                *(ptr++) = '\\';
                *(ptr++) = 't';
                break;
            default:
                *(ptr++) = c;
                break;
            }
        }
        return {bytes, size};
    }

    std::string parseString(const char *data, size_t size)
    {
        std::string string = std::string();
        const char *end = data + size;
        while (data < end)
        {
            if (*data == '\\')
            {
                size_t length;
                size_t count;
                char *chars = escapeChars(data, end, length, count);
                string += std::string_view(chars, count);
                delete[] chars;
                data += length;
            }
            else
            {
                string += *(data++);
            }
        }
        return string;
    }

    char *escapeChars(const char *chars, const char *end, size_t &sequenceLength, size_t &charsCount)
    {
        if (++chars >= end)
        {
            throw ParseError("Missing escape sequence", chars);
        }
        sequenceLength = 2;
        charsCount = 1;
        switch (*chars)
        {
        case '"':
            return new char[]{'"'};
        case '\\':
            return new char[]{'\\'};
        case '/':
            return new char[]{'/'};
        case 'b':
            return new char[]{'\b'};
        case 'f':
            return new char[]{'\f'};
        case 'n':
            return new char[]{'\n'};
        case 'r':
            return new char[]{'\r'};
        case 't':
            return new char[]{'\t'};
        case 'u':
            return parseCodePoint(chars + 1, end, sequenceLength, charsCount);
        default:
            throw ParseError(std::string("Invalid escape sequence \\") + chars[0], chars);
        }
    }

    uint16_t parseHexCode(const char *chars, const char *end)
    {
        if (chars + 3 >= end)
        {
            throw ParseError("Missing hex code", chars);
        }
        uint16_t code = 0;
        try
        {
            code |= parseHexDigit(*(chars++)) << 12;
            code |= parseHexDigit(*(chars++)) << 8;
            code |= parseHexDigit(*(chars++)) << 4;
            code |= parseHexDigit(*(chars++));
        }
        catch(const std::exception& e)
        {
            throw ParseError(e.what(), chars);
        }
        return code;
    }

    char *parseCodePoint(const char *chars, const char *end, size_t &sequenceLength, size_t &charsCount)
    {
        uint32_t codepoint = parseHexCode(chars, end);
        unsigned char *utf8;
        if (codepoint >= 0xd800 && codepoint <= 0xdfff)
        {
            sequenceLength = 12;
            codepoint -= 0xd800;
            codepoint <<= 10;
            if (chars + 5 >= end || chars[4] != '\\' || chars[5] != 'u')
            {
                throw ParseError("Unpaired UTF-16 surrogate", chars - 2);
            }
            uint16_t second = parseHexCode(chars + 6, end);
            if (second < 0xd800 || second > 0xdfff)
            {
                throw ParseError("Unpaired UTF-16 surrogate", chars - 2);
            }
            codepoint += second - 0xdc00;
            codepoint += 0x10000;
            utf8 = new unsigned char[]{0b11110000, 0b10000000, 0b10000000, 0b10000000};
            utf8[0] |= (codepoint & 0b111 << 18) >> 18;
            utf8[1] |= (codepoint & 0b111111 << 12) >> 12;
            utf8[2] |= (codepoint & 0b111111 << 6) >> 6;
            utf8[3] |= codepoint & 0b111111;
            charsCount = 4;
        }
        else if (codepoint >= 0x800)
        {
            sequenceLength = 6;
            utf8 = new unsigned char[]{0b11100000, 0b10000000, 0b10000000};
            utf8[0] |= (codepoint & 0b1111 << 12) >> 12;
            utf8[1] |= (codepoint & 0b111111 << 6) >> 6;
            utf8[2] |= codepoint & 0b111111;
            charsCount = 3;
        }
        else if (codepoint >= 0x80)
        {
            sequenceLength = 6;
            utf8 = new unsigned char[]{0b11000000, 0b10000000};
            utf8[0] |= (codepoint & 0b11111 << 6) >> 6;
            utf8[1] |= codepoint & 0b111111;
            charsCount = 2;
        }
        else
        {
            sequenceLength = 6;
            utf8 = new unsigned char[]{(unsigned char)codepoint};
            charsCount = 1;
        }
        return (char*)utf8;
    }

    unsigned char parseHexDigit(char hex)
    {
        switch (hex)
        {
        case '0':
            return 0;
        case '1':
            return 1;
        case '2':
            return 2;
        case '3':
            return 3;
        case '4':
            return 4;
        case '5':
            return 5;
        case '6':
            return 6;
        case '7':
            return 7;
        case '8':
            return 8;
        case '9':
            return 9;
        case 'a':
        case 'A':
            return 10;
        case 'b':
        case 'B':
            return 11;
        case 'c':
        case 'C':
            return 12;
        case 'd':
        case 'D':
            return 13;
        case 'e':
        case 'E':
            return 14;
        case 'f':
        case 'F':
            return 15;
        default:
            throw std::invalid_argument(std::string("Invalid hex digit '") + hex + '\'');
            break;
        }
    }
}